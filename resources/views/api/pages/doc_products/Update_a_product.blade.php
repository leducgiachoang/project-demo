@extends('api.layouts.pageLayout')

@section('contents_template')

<div class="col-lg-5">
    <h4 class="mt-4">Update a product</h4>
    <p>This API lets you make changes to a product.</p>
    <h4>HTTP request</h4>
    <div class="req_box">
        <div class="req_box_1">PUT</div>
        <div class="req_box_text">/products/{id}</div>
    </div>
</div>
<div class="col-lg-5">
    <div class="mt-4" id="swagger-ui"></div>
    <script>
        window.onload = function() {
          // Build a system
          const ui = SwaggerUIBundle({
            url: './js/products/put.json',
            dom_id: '#swagger-ui',
            {{--  deepLinking: true,
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            plugins: [
              SwaggerUIBundle.plugins.DownloadUrl
            ],
            layout: "StandaloneLayout"  --}}
          })

          window.ui = ui
        }
      </script>
</div>

@endsection
