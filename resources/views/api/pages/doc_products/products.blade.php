@extends('api.layouts.pageLayout')
@section('contents_template')


    <div class="col-lg-5">
        <h4 class="mt-4">Product properties</h4>

        <table class="table mt-2">
            <thead>
              <tr>
                <th scope="col">Attribute</th>
                <th scope="col">Type</th>
                <th scope="col">Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th><span class="badge badge-secondary">id</span></th>
                <td>integer</td>
                <td>Unique identifier for the resource.</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">product_type_id</span></th>
                <td>integer</td>
                <td>Unique transaction ID.</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">site_id</span></th>
                <td>integer</td>
                <td>Unique transaction ID.</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">name</span></th>
                <td>integer</td>
                <td>Product name.</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">sku</span></th>
                <td>integer</td>
                <td>Unique identifier.</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">images</span></th>
                <td>integer</td>
                <td>List of images. See Product - Images properties</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">description</span></th>
                <td>integer</td>
                <td>Product description.</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">permalink</span></th>
                <td>integer</td>
                <td>Product URL.</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">is_custom_design</span></th>
                <td>integer</td>
                <td>Unique transaction ID.</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">store_product_type</span></th>
                <td>integer</td>
                <td>Store product type</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">weight</span></th>
                <td>integer</td>
                <td>Weight product</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">weight_unit</span></th>
                <td>integer</td>
                <td>Weight Unit Product</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">created_at</span></th>
                <td>integer</td>
                <td>Created at product</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">email_desgin</span></th>
                <td>integer</td>
                <td>Email desgin products</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">deleted_at</span></th>
                <td>integer</td>
                <td>Deleted at product</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">is_draft</span></th>
                <td>integer</td>
                <td>Unique transaction ID.</td>
              </tr>

              <tr>
                <th><span class="badge badge-secondary">is_draft_by</span></th>
                <td>integer</td>
                <td>Unique transaction ID.</td>
              </tr>
            </tbody>
          </table>

    </div>
    <div class="col-lg-5">

    </div>

@endsection
