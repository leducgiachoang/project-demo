@extends('api.layouts.pageLayout')
@section('contents_template')

<div class="col-lg-5">
    <h4 class="mt-4">Retrieve a product</h4>
    <p>This API lets you retrieve and view a specific product by ID.</p>

    <h5>HTTP request</h5>

    <div class="req_box">
        <div class="req_box_1">GET</div>
        <div class="req_box_text">/products/{id}</div>
    </div>
</div>
<div class="col-lg-5">
    <div class="mt-4" id="swagger-ui"></div>
    <script>
        window.onload = function() {
          // Build a system
          const ui = SwaggerUIBundle({
            url: './js/products/Retrieve_a_product.blade.json',
            dom_id: '#swagger-ui',
            {{--  deepLinking: true,
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            plugins: [
              SwaggerUIBundle.plugins.DownloadUrl
            ],
            layout: "StandaloneLayout"  --}}
          })

          window.ui = ui
        }
      </script>
</div>

@endsection
