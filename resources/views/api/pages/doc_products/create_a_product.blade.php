@extends('api.layouts.pageLayout')
@section('contents_template')

<div class="col-lg-5">
    <h4 class="mt-4">Create a product</h4>
    <p>This API helps you to create a new product.</p>

    <h5>HTTP request</h5>

    <div class="req_box">
        <div class="req_box_1">POST</div>
        <div class="req_box_text">/products</div>
    </div>
</div>
<div class="col-lg-5">
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {

          // Build a system
          const ui = SwaggerUIBundle({
            url: './js/products/create_a_product.json',
            dom_id: '#swagger-ui',
            {{--  deepLinking: true,
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            plugins: [
              SwaggerUIBundle.plugins.DownloadUrl
            ],
            layout: "StandaloneLayout"  --}}
          })

          window.ui = ui
        }
      </script>
</div>
@endsection
