<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <base href="{{ asset('') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <link rel="stylesheet" href="./css/main.css">
    <!-- Bootstrap CSS -->

    <script src="js/main.js"></script>
    <link rel="stylesheet" href="css/menu.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swagger-ui-dist@3.40.0/swagger-ui.css">

    <script src="https://cdn.jsdelivr.net/npm/swagger-ui-dist@3.40.0/swagger-ui-bundle.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/swagger-ui-dist@3.40.0/swagger-ui-standalone-preset.js"></script>

</head>
  <body>

    
    <div class="container-fluid">
        <div class="row">
        <div class="col-lg-2">
            <button class="menu_mobile_buttom">
                <div>
                    <div><i class="fas fa-align-justify"></i></div>
                    <div>N <br>A <br> V</div>
                </div>
            </button>
            <nav class="nav_left">
                <button class="menu_mobile_buttom_exit">
                    <div>
                        <i class="fas fa-times"></i>
                    </div>
                </button>
                <header class="header_menu">
                    <img src="https://woocommerce.github.io/woocommerce-rest-api-docs/images/logo.png" alt="">
                </header>
                <main class="main-nav-left">
                    <ul class="main-nav-left_ul">
                        <li>
                            <div class="nav-left-a_c1">Products</div>
                            <ul class="nav_left-c2">
                                <li><a href="{{ route('product-properties') }}">Product properties</a></li>
                                <li><a href="{{ route('create_a_product') }}">Create a product</a></li>
                                <li><a href="{{ route('Retrieve_a_product') }}">Retrieve a product</a></li>
                                <li><a href="{{ route('List_all_products') }}">List all products</a></li>
                                <li><a href="{{ route('Update_a_product') }}">Update a product</a></li>
                                <li><a href="{{ route('Delete_a_product') }}">Delete a product</a></li>
                                <li><a href="{{ route('Batch_update_products') }}">Batch update products</a></li>
                            </ul>
                        </li>
                    </ul>
                </main>
            </nav>
        </div>

        @yield('contents_template')


        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
