<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('api.pages.home');
});

Route::group(['prefix' => 'document-api'], function () {
 Route::get('product-properties', 'products_doc@products_doc')->name('product-properties');
 Route::get('create-a-product', 'products_doc@create_a_product')->name('create_a_product');
 Route::get('Retrieve-a-product', 'products_doc@Retrieve_a_product')->name('Retrieve_a_product');
 Route::get('List-all-products', 'products_doc@List_all_products')->name('List_all_products');
 Route::get('Update-a-product', 'products_doc@Update_a_product')->name('Update_a_product');
 Route::get('Delete-a-product', 'products_doc@Delete_a_product')->name('Delete_a_product');
 Route::get('Batch-update-products', 'products_doc@Batch_update_products')->name('Batch_update_products');
});


