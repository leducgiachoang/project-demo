<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class products_doc extends Controller
{
    public function products_doc(){
        return view('api.pages.doc_products.products');
    }

    public function create_a_product(){
        return view('api.pages.doc_products.create_a_product');
    }

    public function Retrieve_a_product(){
        return view('api.pages.doc_products.Retrieve_a_product');
    }

    public function List_all_products(){
        return view('api.pages.doc_products.List_all_products');
    }

    public function Update_a_product(){
        return view('api.pages.doc_products.Update_a_product');
    }

    public function Delete_a_product(){
    return view('api.pages.doc_products.Delete_a_product');
    }

    public function Batch_update_products(){
    return view('api.pages.doc_products.Batch_update_products');
    }

}
